# WiFi Mobility #

This is a mac80211 patch which can be used to eliminate the handoff process in 802.11 and maintain active TCP streams in the case of mobile clients, effectively granting mobility over wireless networks by constantly switching channels.

### Summary ###

* One device, multi-channel
* Can be used with network-manager
* Vendor-independent - all modifications are within mac80211 and cfg80211
* Dynamic - channel switching is triggered when associating with multiple access points

### Requirements ###

This has been tested on Ubuntu 14.04.02 LTS using an Atheros 9k.

* Download and install [Ubuntu 14.04 LTS](http://www.ubuntu.com/download/desktop)
* Set up [MPTCP](http://multipath-tcp.org/pmwiki.php/Users/HowToInstallMPTCP?) and reboot

### Setting up ###

#### Building ####

We recommend turning off wireless before the following step.

```
#!bash
sudo ./wm build && ./wm unload && ./wm load
```
Now, turn wireless back on.

#### Adding virtual interfaces ####

```
#!bash
./wm vifs add <wireless_interface_name> <index>
```

Vif names will typically be of the form `v#_wlan%`. For example:

```
#!bash
./wm vifs add wlan1 5
```

Will add `v5_wlan1` to the list of interfaces.

Each new virtual interface will be used to associate to a different SSID. NetworkManager will automatically bring it up and attempt to associate.

##### Notable caveat #####

By using **wm** to add virtual interfaces, a new MAC address is given, derived from the base interface MAC address. For some reason, NetworkManager reverts this MAC address if association fails.

A workaround for this is to stop network-manager before loading mac80211 and your wireless driver via
```
#!bash
service network-manager stop
```

You can then add vifs and set new MACs and then restart network-manager. It will take over the new MAC and will not change it.

#### Set up routing ####

After testing connectivity through each interface and access point, set up [MPTCP routing](http://multipath-tcp.org/pmwiki.php/Users/ConfigureRouting).

### Future updates ###

* Automatic SSID association to ensure "hands-free" operation
* Throughput maximization
* Optimal SSID choice

### Contact ###

* **Vladimir Diaconescu** - vladimirdiaconescu[at]yahoo.com
* **Costin Raiciu** - costin.raiciu[at]cs.pub.ro
* **Dragos Niculescu** - dragos.niculescu[at]cs.pub.ro